export function setup(ctx) {
    class SkillTracker {
        constructor(body) {
            this.skill = undefined;
            this.started = false;
            this.startTime = 0;

            this.startSkillXP = 0;
            this.startAbyssalXP = 0;

            this.rateSkillXP = 0;
            this.rateAbyssalXP = 0;

            this.skillLevelContainers = [];
            this.abyssalLevelContainers = [];

            const content = new DocumentFragment();
            content.append(getTemplateNode('xp-per-hour-dropdown-skill-template'));
            this.row = getAnyElementFromFragment(content, 'row');
            this.skillImage = getAnyElementFromFragment(content, 'skill-image');

            for (let i = 0; i < 4; i++) {
                this.skillLevelContainers.push(getAnyElementFromFragment(content, `skill-level-container-${i}`));
                this.abyssalLevelContainers.push(getAnyElementFromFragment(content, `abyssal-level-container-${i}`));
            }

            this.skillLevel = getAnyElementFromFragment(content, 'skill-level');
            this.skillXP = getAnyElementFromFragment(content, 'skill-xp');
            this.skillTime = getAnyElementFromFragment(content, 'skill-time');
            this.skillTarget = getAnyElementFromFragment(content, 'skill-target');

            this.abyssalLevel = getAnyElementFromFragment(content, 'abyssal-level');
            this.abyssalXP = getAnyElementFromFragment(content, 'abyssal-xp');
            this.abyssalTime = getAnyElementFromFragment(content, 'abyssal-time');
            this.abyssalTarget = getAnyElementFromFragment(content, 'abyssal-target');

            body.append(this.row);
        }

        setSkill(skill) {
            this.skill = skill;
            this.skillImage.src = skill.media;
            this.startTime = Date.now();
            this.started = false;

            this.startSkillXP = this.skill.xp;
            this.startAbyssalXP = this.skill.abyssalXP;

            this.rateSkillXP = 0;
            this.rateAbyssalXP = 0;

            this.skillTarget.value = (game.settings.showVirtualLevels ? this.skill.virtualLevel : this.skill.level) + 1;
            this.abyssalTarget.value = (game.settings.showVirtualLevels ? this.skill.virtualAbyssalLevel : this.skill.abyssalLevel) + 1;

            this.update();
            this.updateLevelVisibility();
            this.updateAbyssalLevelVisibility();
        }

        get shouldShow() {
            return this.rateSkillXP > 0 || this.rateAbyssalXP > 0;
        }

        update() {
            // Start Time Tracking on first XP Gain, not script load.
            if(!this.started && (this.skill.xp > this.startSkillXP || this.skill.abyssalXP > this.startAbyssalXP)) {
                this.started = true;
                this.startTime = Date.now();
            }

            this.updateSkill();
            this.updateAbyssal();

            if (this.shouldShow) {
                showElement(this.row);
            }
            else {
                hideElement(this.row);
            }
        }

        // standard
        updateSkill() {
            const level = game.settings.showVirtualLevels ? this.skill.virtualLevel : this.skill.level;
            const xp = this.skill.xp;

            const gainedXP = xp - this.startSkillXP;

            if (gainedXP > 0) {
                const time = Date.now() - this.startTime;
                this.rateSkillXP = gainedXP / time;

                let targetLevel = Number(this.skillTarget.value);
                if (targetLevel <= level) {
                    targetLevel = level + 1;
                    this.skillTarget.value = targetLevel;
                }

                if (isNaN(targetLevel))
                    targetLevel = level + 1;

                const timeInMS = (exp.levelToXP(targetLevel) - xp) / this.rateSkillXP;

                this.skillXP.textContent = `${numberWithCommas(Math.floor(this.rateSkillXP * 3600000))}`;
                this.skillTime.textContent = formatAsShorthandTimePeriod(timeInMS);
            }
            else {
                this.skillXP.textContent = this.skillTime.textContent = `---`;
            }

            this.skillLevel.textContent = `${level} / ${this.skill.currentLevelCap}`;
        }

        updateLevelVisibility() {
            if (this.skill.shouldShowStandardLevels) {
                this.skillLevelContainers.forEach(showElement);
            }
            else {
                this.skillLevelContainers.forEach(hideElement);
            }
        }

        // abyssal
        updateAbyssal() {
            const level = game.settings.showVirtualLevels ? this.skill.virtualAbyssalLevel : this.skill.abyssalLevel;
            const xp = this.skill.abyssalXP;

            const gainedXP = xp - this.startAbyssalXP;

            if (gainedXP > 0) {
                const time = Date.now() - this.startTime;
                this.rateAbyssalXP = gainedXP / time;

                let targetLevel = Number(this.abyssalTarget.value);
                if (targetLevel <= level) {
                    targetLevel = level + 1;
                    this.abyssalTarget.value = targetLevel;
                }

                if (isNaN(targetLevel))
                    targetLevel = level + 1;

                const timeInMS = (abyssalExp.levelToXP(targetLevel) - xp) / this.rateAbyssalXP;

                this.abyssalXP.textContent = `${numberWithCommas(Math.floor(this.rateAbyssalXP * 3600000))}`;
                this.abyssalTime.textContent = formatAsShorthandTimePeriod(timeInMS);
            }
            else {
                this.abyssalXP.textContent = this.abyssalTime.textContent = `---`;
            }

            this.abyssalLevel.textContent = `${level} / ${this.skill.currentAbyssalLevelCap}`;
        }

        updateAbyssalLevelVisibility() {
            if (this.skill.shouldShowAbyssalLevels) {
                this.abyssalLevelContainers.forEach(showElement);
            }
            else {
                this.abyssalLevelContainers.forEach(hideElement);
            }
        }
    }

    class SkillTrackerMenu {
        constructor() {
            this.tableRows = new Map();

            const content = new DocumentFragment();
            content.append(getTemplateNode('xp-per-hour-dropdown-template'));

            this.dropdown = getAnyElementFromFragment(content, 'header-xp-dropdown');
            this.tableBody = getAnyElementFromFragment(content, 'table-body');
            this.tableOptions = getAnyElementFromFragment(content, 'options');

            game.skills.forEach((skill) => {
                const row = new SkillTracker(this.tableBody);
                row.setSkill(skill);
                this.tableRows.set(skill, row);
            });

            this.dropdown.onclick = (e) => { e.stopPropagation(); }
            this.tableOptions.onclick = (e) => this.options();

            const button = getAnyElementFromFragment(content, 'header-button');
            const target = document.getElementById('page-header-potions-dropdown').parentElement;
            target.parentElement.insertBefore(button, target);

            this.updater = setInterval(() => { this.update(); }, 1000);
        }

        update() {
            this.tableRows.forEach((tracker, skill) => {
                tracker.update();
            });
        }

        reset() {
            this.tableRows.forEach((tracker, skill) => {
                tracker.setSkill(skill);
            });
        }

        options() {
            const content = new DocumentFragment();
            content.append(getTemplateNode('xp-per-hour-options-template'));

            const rootNode = getAnyElementFromFragment(content, 'options-root');
            const tableBody = getAnyElementFromFragment(content, 'table-body');
            const allReset = getAnyElementFromFragment(content, 'reset');

            allReset.onclick = (e) => {
                e.stopPropagation();
                this.reset();
            }

            game.skills.forEach((skill) => {
                const tracker = this.tableRows.get(skill);

                const rowFrag = new DocumentFragment();
                rowFrag.append(getTemplateNode('xp-per-hour-options-skill-template'));

                const row = getAnyElementFromFragment(rowFrag, 'row');
                const skillImage = getAnyElementFromFragment(rowFrag, 'skill-image');
                const skillName = getAnyElementFromFragment(rowFrag, 'skill-name');
                const skillReset = getAnyElementFromFragment(rowFrag, 'reset');

                skillImage.src = skill.media;
                skillName.textContent = skill.name;
                skillReset.onclick = (e) => {
                    e.stopPropagation();
                    tracker.setSkill(skill);
                }

                tableBody.append(row);
            });

            SwalLocale.fire({
                title: "XP/H Options",
                html: rootNode,
            });
        }
    }

    ctx.onInterfaceReady((ctx) => {
        new SkillTrackerMenu();
    });
}